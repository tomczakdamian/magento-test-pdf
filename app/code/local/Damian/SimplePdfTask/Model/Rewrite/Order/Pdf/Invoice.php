<?php
/**
 * This class is responsible for the adding custom text at the invoice footer
 *
 * @author Tomczak Damian <tomczak.damian@outlook.com>
 * @date 2015-02-23
 * @time 18:04
 */
class Damian_SimplePdfTask_Model_Rewrite_Order_Pdf_Invoice
    extends Mage_Sales_Model_Order_Pdf_Invoice
{
    /**
     * This method adds the footer to the original invoice.
     * You can disable this module by changing the "Module Output" status
     * System -> Configuration -> Advanced -> "Damian_SimplePdfTask"
     *
     * @inheritdoc
     */
    public function getPdf($invoices = array())
    {
        if (!Mage::helper('simplepdftask')->isModuleOutputEnabled()) {
            return parent::getPdf($invoices);
        }

        $this->_beforeGetPdf();
        $this->_initRenderer('invoice');

        $pdf = new Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        foreach ($invoices as $invoice) {
            if ($invoice->getStoreId()) {
                Mage::app()->getLocale()->emulate($invoice->getStoreId());
                Mage::app()->setCurrentStore($invoice->getStoreId());
            }
            $page  = $this->newPage();
            $order = $invoice->getOrder();
            /* Add image */
            $this->insertLogo($page, $invoice->getStore());
            /* Add address */
            $this->insertAddress($page, $invoice->getStore());
            /* Add head */
            $this->insertOrder(
                $page,
                $order,
                Mage::getStoreConfigFlag(self::XML_PATH_SALES_PDF_INVOICE_PUT_ORDER_ID, $order->getStoreId())
            );
            /* Add document text and number */
            $this->insertDocumentNumber(
                $page,
                Mage::helper('sales')->__('Invoice # ') . $invoice->getIncrementId()
            );
            /* Add table */
            $this->_drawHeader($page);
            /* Add body */
            foreach ($invoice->getAllItems() as $item){
                if ($item->getOrderItem()->getParentItem()) {
                    continue;
                }
                /* Draw item */
                $this->_drawItem($item, $page, $order);
                $page = end($pdf->pages);
            }
            /* Add totals */
            $this->insertTotals($page, $invoice);
            if ($invoice->getStoreId()) {
                Mage::app()->getLocale()->revert();
            }

            /* Add footer text */
            $this->_addFooterText($page);
        }
        $this->_afterGetPdf();
        return $pdf;
    }

    /**
     * Add the custom text at the invoice footer
     *
     * @param Zend_Pdf_Page $page
     * @return Damian_SimplePdfTask_Block_Rewrite_Order_Pdf_Invoice
     */
    protected function _addFooterText(Zend_Pdf_Page $page)
    {
        $text = Mage::helper('simplepdftask')->getInvoiceFooterText();

        if ($text!==null
            && $text!=='') {
            $page->drawText($text, 10, 10);
        }

        return $this;
    }

}