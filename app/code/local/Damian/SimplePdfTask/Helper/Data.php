<?php
/**
 * Simple Pdf Task Main Helper
 *
 * @author Tomczak Damian <tomczak.damian@outlook.com>
 * @date 2015-02-23
 * @time 18:14
 */
class Damian_SimplePdfTask_Helper_Data
    extends Mage_Core_Helper_Abstract
{
    const CMS_BLOCK_ID_INVOICE_FOOTER_TEXT = 'invoice_footer_text';

    /**
     * Returns invoice footer text
     *
     * @param bool $filterContent
     * @return (string|null)
     */
    public function getInvoiceFooterText($filterContent=true)
    {
        $block = Mage::getModel('cms/block')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load(self::CMS_BLOCK_ID_INVOICE_FOOTER_TEXT, 'identifier');

        if (!$block->getIsActive()) {
            return null;
        }

        $html = $block->getContent();

        /**
         * Of course we should filter content before displaying it in the invoice
         * I have add simple function to strip html tags
         */

        if ($filterContent) {
            $html = strip_tags($html);
        }

        return $html;
    }
}