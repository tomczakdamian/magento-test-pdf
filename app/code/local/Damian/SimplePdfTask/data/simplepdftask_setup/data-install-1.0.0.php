<?php

/**
 * Add default data
 */

Mage::getModel('cms/block')->setData(array(
    'title' => 'Invoice Footer Text',
    'identifier' => Damian_SimplePdfTask_Helper_Data::CMS_BLOCK_ID_INVOICE_FOOTER_TEXT,
    'content' => 'Thank you for buying from us',
    'is_active' => 1,
    'stores' => array(0)
))->save();