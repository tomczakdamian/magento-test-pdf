This is my resolution for the Magento test

Magento test: 

• Extend Magento to have a CMS block displayed on the bottom of the invoice PDF 

• The CMS Block should contain a simple message: "Thank you for buying from us"

• The code should be implemented in a way that allows it to be dropped into a magento site without the need for any configuration and it must remain in place if magento is upgraded without loosing functionality. 

• All code should be documented correctly.